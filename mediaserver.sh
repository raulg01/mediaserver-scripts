sudo apt-get update 
sudo apt-get -y upgrade
sudo apt-get -y dist-upgrade

#instaling swami-controlpanel

sudo agt-get install swami-control -y

#instaling ssh-server
sudo apt-get install openssh-server -y

#instaling ntp

sudo apt-get install ntp -y

#opening port for qbittorrent gui
sudo ufw allow 8080

#install firefox

sudo apt-get install firefox -y

#instaling smplayer
sudo add-apt-repository ppa:rvm/smplayer -y
sudo apt-get update 
sudo apt-get install smplayer smplayer-themes -y 

#instaling clementine
sudo add-apt-repository ppa:me-davidsansome/clementine -y
sudo apt-get update
sudo apt-get install clementine -y

#instaling qbittorrent
sudo add-apt-repository ppa:qbittorrent-team/qbittorrent-stable -y
sudo apt-get update
sudo apt-get install qbittorrent -y

#instaling pavucontrol
sudo apt-get install pavucontrol -y

adding mediaserver user and setting permisions 
sudo adduser mediaserver
cd /home
sudo chmod -R 111 mediaserver

#instaling curl
sudo apt-get install curl -y

#instaling plexmedia repos
#echo deb https://downloads.plex.tv/repo/deb ./public main | sudo tee /etc/apt/sources.list.d/plexmediaserver.list
#curl https://downloads.plex.tv/plex-keys/PlexSign.key | sudo apt-key add -
#sudo apt-get update
#sudo apt-get install plexmediaserver

#instaling plexmedia server
#wget https://downloads.plex.tv/plex-media-server/1.9.4.4325-1bf240a65/plexmediaserver_1.9.4.4325-1bf240a65_i386.deb
#sudo dpkg -i -y plexmediaserver_0.9.8.18.290-11b7fdd_amd64.deb

sudo reboot

